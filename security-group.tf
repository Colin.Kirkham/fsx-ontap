resource "aws_security_group" "ssm" {
  name        = "private-aws-network-ssm"
  description = "Allow IPv4 HTTPS traffic for SSM"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "HTTPS for SSM"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.vpc.cidr_block]
  }
}

resource "aws_security_group" "ec2" {
  name        = "private-aws-ec2-sg"
  description = "Base EC2 security group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "SSM ingress"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups = [aws_security_group.ssm.id]
  }

  egress {
    description = "SSM egress"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups = [aws_security_group.ssm.id]
  }

  ingress {
    description = "NFS ingress"
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "NFS egress"
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}