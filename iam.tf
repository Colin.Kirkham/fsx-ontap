# Role for instances to use SSM and CloudWatch agent
resource "aws_iam_role" "ssm_role" {
  name = "private-aws-network-ssm-policy-role"

  assume_role_policy = jsonencode(
    {
      "Version" : "2008-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : ["ec2.amazonaws.com"]
          },
          "Effect" : "Allow"
        }
      ]
  })
}

# Create a profile for the role
resource "aws_iam_instance_profile" "ssm_profile" {
  name = "private-aws-network-ssm-profile"
  path = "/system/"
  role = aws_iam_role.ssm_role.name
}

# Attach the ssm managed instance core policy to the role
resource "aws_iam_role_policy_attachment" "amazon_ssm_managed_instance_core" {
  role       = aws_iam_role.ssm_role.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

# Attach the ssm read only policy to the role
resource "aws_iam_role_policy_attachment" "amazon_ssm_read_only_access" {
  role       = aws_iam_role.ssm_role.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"
}

# Attach the cloudwatch agent policy to the role
resource "aws_iam_role_policy_attachment" "cloudwatch_agent_server_policy" {
  role       = aws_iam_role.ssm_role.id
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}
