data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "instance" {
  ami                  = data.aws_ami.amazon_linux_2.id
  instance_type        = "t3.micro"
  iam_instance_profile = aws_iam_instance_profile.ssm_profile.id
  subnet_id            = aws_subnet.primary_subnet.id
  availability_zone    = "${var.aws_region}a"
  security_groups      = [aws_security_group.ec2.id]

  tags = {
    Name = "Private AWS Network Instance"
  }
}

output "instance_id" {
  value = aws_instance.instance.id
}
