variable "aws_region" {
  type        = string
  default     = "eu-west-2"
  description = "The region in which to create the vpc"
}
