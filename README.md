# FSX-ONTAP
1. First run `terraform init`
2. Then `terraform plan`
3. Then `terraform apply`

This will result in ouput similar to this:

```
instance_id = "i-0b00a9eaf7f17a91b"
ontap_nfs_endpoint = [
  tolist([
    "svm-02d8bae88a07c0809.fs-0419d83dc98cee440.fsx.eu-west-2.amazonaws.com",
  ]),
]
```

This will create a VPC with two subnets in the London region, create a 1TB ONTAP drive and an instance that can connect to it, plus allow connections via Session Manager.

To mount the drive, connect to the instance id which is output at the end of the terraform process using the web console using the Session Manager tab.

Next run the following commands:

1. `sudo mkdir -p /opt/fsx/volume1`
2. `sudo mkdir -p /opt/fsx/volume2`
3. `sudo mount -t nfs <endpoint_url>:/volume1 /opt/fsx/volume1`
4. `sudo mount -t nfs <endpoint_url>:/volume2 /opt/fsx/volume2`

where `<endpoint_url>` comes from the output from the *terraform* script above.

I have only opened the NFS tcp port in the security group which appears to work just fine but may not support some features. A more exhaustive list can be found here: https://docs.aws.amazon.com/fsx/latest/ONTAPGuide/limit-access-security-groups.html

The mount can also be added to the *fstab* file as normal. Details on how to do this can be found here: https://docs.aws.amazon.com/fsx/latest/ONTAPGuide/attach-volumes.html

The file `ontap.tf` is the most important one here. I've left the other options in but commented out so you can see what else is supported.

There are two volumes created, volume1 with no tiering policy and volume2 with a standard tiering policy. This works at the block level rather than the file level so may be appropriate even though we can't use similar functionality in EFS. Definitely worth testing it out as the cost saving would be significant.
