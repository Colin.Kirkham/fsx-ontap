resource "aws_fsx_ontap_file_system" "test_ontap_fs" {
  storage_capacity                  = 1024
  subnet_ids                        = [aws_subnet.primary_subnet.id, aws_subnet.secondary_subnet.id]
  preferred_subnet_id               = aws_subnet.primary_subnet.id
  deployment_type                   = "MULTI_AZ_1"
  automatic_backup_retention_days   = 30
  storage_type                      = "SSD"
  throughput_capacity               = 512
  security_group_ids                = [aws_security_group.ec2.id]
#   weekly_maintenance_start_time     = "d:HH:MM"
#   kms_key_id                        = ""
#   daily_automatic_backup_start_time = "HH:MM"
#   disk_iops_configuration {
#     iops = 1024
#     mode = "AUTOMATIC|USER_PROVISIONED"
#   }
#   endpoint_ip_address_range = "xxx.xxx.xxx.xxx"
#   fsx_admin_password        = "50mePa55w0rd"
#   route_table_ids           = []
  tags = {
    Name = "Test FSx ONTAP File System"
  }
}

resource "aws_fsx_ontap_storage_virtual_machine" "test_ontap_svm" {
  file_system_id             = aws_fsx_ontap_file_system.test_ontap_fs.id
  name                       = "test_ontap_svm"
  root_volume_security_style = "UNIX"
  tags = {
      Name = "Test FSx ONTAP SVM"
  }
}

output "ontap_nfs_endpoint" {
  value = [for d in aws_fsx_ontap_storage_virtual_machine.test_ontap_svm.endpoints : d.nfs.*.dns_name]
}

resource "aws_fsx_ontap_volume" "test_ontap_volume1" {
  name                       = "volume1"
  junction_path              = "/volume1"
  size_in_megabytes          = 512
  storage_efficiency_enabled = true
  storage_virtual_machine_id = aws_fsx_ontap_storage_virtual_machine.test_ontap_svm.id
  tiering_policy {
    name = "NONE"
  }
}

resource "aws_fsx_ontap_volume" "test_ontap_volume2" {
  name                       = "volume2"
  junction_path              = "/volume2"
  size_in_megabytes          = 512
  storage_efficiency_enabled = false
  storage_virtual_machine_id = aws_fsx_ontap_storage_virtual_machine.test_ontap_svm.id
  tiering_policy {
    name = "AUTO"
    cooling_period = 31
  }
}
